/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phi_courses.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/09 11:40:02 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/23 09:09:23 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"
#include "phi_pacing.h"
#include "phi_check_in.h"

void	statement(const char *msg, t_banquet *bqt, bool closing_statement)
{
	sem_wait(bqt->floor);
	printf("%lli %i %s\n", timestamp_ms() - bqt->genesis, bqt->phil.key, msg);
	if (!closing_statement)
		sem_post(bqt->floor);
}

static void	digestion(t_banquet *bqt)
{
	statement("is sleeping", bqt, false);
	punctilious_sleep(bqt->nap_time);
	statement("is thinking", bqt, false);
}

static void	nourishment(t_banquet *bqt)
{
	sem_wait(bqt->forks);
	statement("has taken a fork", bqt, false);
	sem_wait(bqt->forks);
	statement("has taken a fork", bqt, false);
	statement("is eating", bqt, false);
	bqt->phil.previous_meal = timestamp_ms();
	punctilious_sleep(bqt->meal_time);
	sem_post(bqt->forks);
	sem_post(bqt->forks);
	bqt->meal_cap--;
	if (bqt->meal_cap == 0)
		sem_post(bqt->satiated);
}

void	*feast(t_banquet *bqt)
{
	pthread_t	tid;

	sem_wait(bqt->live);
	bqt->genesis = timestamp_ms();
	bqt->phil.previous_meal = bqt->genesis;
	if (pthread_create(&tid, NULL, starvation_monitor, bqt))
		exit(EXIT_FAILURE);
	if (bqt->phil.key % 2 == 0)
		punctilious_sleep(bqt->meal_time);
	while (true)
	{
		nourishment(bqt);
		digestion(bqt);
	}
	exit(EXIT_SUCCESS);
}

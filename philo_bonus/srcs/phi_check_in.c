/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phi_check_in.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/10 16:44:49 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/23 08:50:39 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"
#include "phi_pacing.h"
#include "phi_courses.h"

void	*starvation_monitor(void *data)
{
	t_banquet	*bqt;

	bqt = (t_banquet *)data;
	while (true)
	{
		if (timestamp_ms() - bqt->phil.previous_meal > bqt->fasting_endurance)
		{
			statement("died", bqt, true);
			sem_post(bqt->done);
			break ;
		}
		usleep(50);
	}
	return (NULL);
}

static void	*servings_monitor(void *data)
{
	t_banquet	*bqt;
	int			i;

	bqt = (t_banquet *)data;
	i = bqt->phils_count;
	while (i--)
		sem_wait(bqt->satiated);
	sem_post(bqt->done);
	return (NULL);
}

void	table_check(t_banquet *bqt)
{
	int			i;
	pthread_t	thread;

	if (pthread_create(&thread, NULL, servings_monitor, bqt))
		exit(EXIT_FAILURE);
	sem_wait(bqt->done);
	i = bqt->phils_count;
	while (i--)
	{
		sem_post(bqt->satiated);
		kill(bqt->phils_pids[i], SIGKILL);
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosophers.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/08 12:25:27 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/23 08:53:41 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"
#include "phi_parsing.h"
#include "phi_utils.h"
#include "phi_pacing.h"
#include "phi_courses.h"
#include "phi_check_in.h"

static t_error	clear_banquet(t_banquet *bqt, t_error cause)
{
	free(bqt->phils_pids);
	sem_close(bqt->forks);
	sem_close(bqt->floor);
	sem_close(bqt->live);
	sem_close(bqt->satiated);
	sem_close(bqt->done);
	return (cause);
}

static t_error	set_table(t_banquet *bqt)
{
	bqt->forks = ez_sem_init("forks", bqt->phils_count);
	bqt->floor = ez_sem_init("floor", 1);
	bqt->live = ez_sem_init("live", 0);
	bqt->satiated = ez_sem_init("satiated", 0);
	bqt->done = ez_sem_init("done", 0);
	if (bqt->forks == SEM_FAILED || bqt->floor == SEM_FAILED
		|| bqt->live == SEM_FAILED)
		return (ERROR);
	return (CLEAR);
}

static t_error	introduce_guests(t_banquet *banquet)
{
	pid_t	pid;
	int		i;

	banquet->phils_pids = malloc(sizeof(pid_t) * banquet->phils_count);
	if (!banquet->phils_pids)
		return (MEM_ALLOC);
	i = 0;
	while (i < banquet->phils_count)
	{
		pid = fork();
		if (pid == -1)
			return (ERROR);
		else if (pid == 0)
		{
			banquet->phil.key = i + 1;
			feast(banquet);
		}
		else
			banquet->phils_pids[i] = pid;
		i++;
	}
	return (CLEAR);
}

int	main(int ac, char **av)
{
	t_banquet	bqt;
	int			i;

	if (parse_args(&bqt, ac, av) != CLEAR)
		return (EXIT_FAILURE);
	if (bqt.meal_cap == 0)
		return (EXIT_SUCCESS);
	if (set_table(&bqt) != CLEAR)
		return (clear_banquet(&bqt, PARSE));
	if (introduce_guests(&bqt) == CLEAR)
	{
		i = 0;
		while (i < bqt.phils_count)
		{
			sem_post(bqt.live);
			i++;
		}
	}
	table_check(&bqt);
	return (clear_banquet(&bqt, CLEAR));
}

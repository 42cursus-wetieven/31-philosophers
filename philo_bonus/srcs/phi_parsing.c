/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phi_parsing.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/16 08:12:18 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/22 14:21:27 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include "philosophers.h"

t_error	prs_atoi(int *res, char *str, int min, int max)
{
	int	sign;

	sign = 1;
	*res = 0;
	while ((*str >= 9 && *str <= 13) || *str == ' ')
		str++;
	if (*str == '-' || *str == '+')
		if (*str++ == '-')
			sign *= -1;
	while ('0' <= *str && *str <= '9' && *str)
	{
		*res = *res * 10 + (*str - 48);
		str++;
	}
	*res *= sign;
	if (*res < min || *res > max || *str != '\0')
		return (PARSE);
	return (CLEAR);
}

t_error	parse_args(t_banquet *bqt, int ac, char **av)
{
	if (ac < 5 || ac > 6)
		return (PARSE);
	if (prs_atoi(&bqt->phils_count, av[1], 1, INT_MAX) != CLEAR)
		return (PARSE);
	if (prs_atoi(&bqt->fasting_endurance, av[2], 0, INT_MAX) != CLEAR)
		return (PARSE);
	if (prs_atoi(&bqt->meal_time, av[3], 0, INT_MAX) != CLEAR)
		return (PARSE);
	if (prs_atoi(&bqt->nap_time, av[4], 0, INT_MAX) != CLEAR)
		return (PARSE);
	if (ac == 6)
	{
		if (prs_atoi(&bqt->meal_cap, av[5], 1, INT_MAX) != CLEAR)
			return (PARSE);
	}
	else
		bqt->meal_cap = -1;
	return (CLEAR);
}

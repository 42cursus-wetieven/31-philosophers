/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phi_utils.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/23 08:52:56 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/23 08:54:52 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

sem_t	*ft_sem_init(char *sem_name, int oflag, mode_t rights, int bandwidth)
{
	sem_t	*sem;

	sem_unlink(sem_name);
	sem = sem_open(sem_name, oflag, rights, bandwidth);
	return (sem);
}

sem_t	*ez_sem_init(char *sem_name, int bandwidth)
{
	return (ft_sem_init(sem_name, O_CREAT | O_EXCL, S_IRWXU, bandwidth));
}

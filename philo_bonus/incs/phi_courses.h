/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phi_courses.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/10 17:25:47 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/22 15:43:33 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHI_COURSES_H
# define PHI_COURSES_H

void	*feast(t_banquet *bqt);
void	statement(const char *msg, t_banquet *bqt, bool closing_statement);

#endif

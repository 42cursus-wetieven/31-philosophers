/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phi_check_in.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/10 17:27:31 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/23 08:51:17 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHI_CHECK_IN_H
# define PHI_CHECK_IN_H

void	*starvation_monitor(void *data);
void	table_check(t_banquet *bqt);

#endif

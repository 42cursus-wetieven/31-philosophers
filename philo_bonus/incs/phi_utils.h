/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phi_utils.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/23 08:53:52 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/23 08:54:46 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHI_UTILS_H
# define PHI_UTILS_H

sem_t	*ft_sem_init(char *sem_name, int oflag, mode_t rights, int bandwidth);
sem_t	*ez_sem_init(char *sem_name, int bandwidth);

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosophers.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/08 09:11:42 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/22 19:35:10 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILOSOPHERS_H
# define PHILOSOPHERS_H

# include <unistd.h>
# include <stdlib.h>
# include <stdbool.h>
# include <stdio.h>
# include <sys/time.h>
# include <sys/wait.h>
# include <semaphore.h>
# include <fcntl.h>
# include <pthread.h>
# include <signal.h>

typedef enum e_fid {
	PARSING,
	TABLESETTING,
	RECEPTION,
	FEASTING
}	t_fid;

typedef enum e_error {
	CLEAR,
	ERROR,
	MEM_ALLOC,
	PARSE,
}	t_error;

typedef struct s_philos {
	int				key;
	long long		previous_meal;
}	t_philos;

// Why forks and not chopsticks guys ? This isn't sound table craft.
typedef struct s_banquet {
	sem_t			*live;
	long long		genesis;
	int				phils_count;
	int				fasting_endurance;
	int				meal_time;
	int				nap_time;
	int				meal_cap;
	sem_t			*forks;
	sem_t			*floor;
	pid_t			*phils_pids;
	t_philos		phil;
	sem_t			*satiated;
	sem_t			*done;
}	t_banquet;

#endif

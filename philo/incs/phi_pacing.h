/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phi_pacing.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/09 12:45:36 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/21 18:08:11 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHI_PACING_H
# define PHI_PACING_H

long long	timestamp_ms(void);
void		punctilious_sleep(long long target, t_banquet *bqt);

#endif

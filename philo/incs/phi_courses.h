/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phi_courses.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/10 17:25:47 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/16 16:37:32 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHI_COURSES_H
# define PHI_COURSES_H

void	*feast(void *philosopher);
void	statement(const char *msg, t_philos *phil, bool closing_statement);

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosophers.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/08 09:11:42 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/19 16:54:10 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILOSOPHERS_H
# define PHILOSOPHERS_H

# include <stdbool.h>
# include <stdlib.h>
# include <unistd.h>
# include <stdio.h>
# include <pthread.h>
# include <sys/time.h>

typedef enum e_fid {
	PARSING,
	TABLESETTING,
	RECEPTION,
	FEASTING
}	t_fid;

typedef enum e_error {
	CLEAR,
	ERROR,
	MEM_ALLOC,
	PARSE,
}	t_error;

// Why forks and not chopsticks guys ? This isn't sound table craft.
typedef struct s_banquet {
	int				phils_count;
	int				fasting_endurance;
	int				meal_time;
	int				nap_time;
	int				meal_cap;
	pthread_mutex_t	*forks;
	pthread_mutex_t	floor;
	long long		genesis;
	bool			live;
}	t_banquet;

typedef struct s_philos {
	t_banquet		*bqt;
	int				key;
	int				meal_count;
	long long		previous_meal;
	pthread_mutex_t	eating;
	pthread_t		thread;
}	t_philos;

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phi_parsing.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/16 08:20:48 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/16 08:21:37 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHI_PARSING_H
# define PHI_PARSING_H

t_error	parse_args(t_banquet *bqt, int ac, char **av);

#endif

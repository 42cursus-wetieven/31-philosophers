/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosophers.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/08 12:25:27 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/21 12:32:35 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"
#include "phi_parsing.h"
#include "phi_pacing.h"
#include "phi_courses.h"
#include "phi_check_in.h"

static void	phi_join_threads(t_banquet *bqt, t_philos *phils)
{
	int	i;

	i = 0;
	while (i < bqt->phils_count)
	{
		if (phils[i].thread)
			pthread_join(phils[i].thread, NULL);
		i++;
	}
}

static t_error	phi_shutdown(t_banquet *bqt, t_philos *phils,
								t_error cause, t_fid fid)
{
	int	i;

	if (cause == CLEAR)
		phi_join_threads(bqt, phils);
	if (fid >= TABLESETTING)
	{
		i = 0;
		while (i < bqt->phils_count)
			if (&bqt->forks[i])
				pthread_mutex_destroy(&bqt->forks[i++]);
		if (bqt->forks)
			free(bqt->forks);
		pthread_mutex_destroy(&bqt->floor);
	}
	if (fid >= RECEPTION)
	{
		i = 0;
		while (i < bqt->phils_count)
			if (&(phils[i].eating))
				pthread_mutex_destroy(&phils[i++].eating);
		if (phils)
			free(phils);
	}
	return (cause);
}

static t_error	set_table(t_banquet *bqt)
{
	int	i;

	bqt->forks = malloc(sizeof(pthread_mutex_t) * bqt->phils_count);
	if (!bqt->forks)
		return (MEM_ALLOC);
	i = 0;
	while (i < bqt->phils_count)
	{
		if (pthread_mutex_init(&bqt->forks[i], NULL) != 0)
		{
			while (--i >= 0)
				pthread_mutex_destroy(&bqt->forks[i]);
			return (MEM_ALLOC);
		}
		i++;
	}
	if (pthread_mutex_init(&bqt->floor, NULL))
		return (MEM_ALLOC);
	return (CLEAR);
}

static t_error	introduce_guests(t_philos **phils, t_banquet *banquet)
{
	int	i;

	*phils = malloc(sizeof(t_philos) * banquet->phils_count);
	if (!*phils)
		return (MEM_ALLOC);
	i = 0;
	while (i < banquet->phils_count)
	{
		(*phils)[i].bqt = banquet;
		(*phils)[i].key = i + 1;
		(*phils)[i].meal_count = 0;
		(*phils)[i].previous_meal = timestamp_ms();
		if (pthread_mutex_init(&(*phils)[i].eating, NULL) != 0)
			return (MEM_ALLOC);
		if (pthread_create(&(*phils)[i].thread, NULL, feast, &(*phils)[i]) != 0)
			return (MEM_ALLOC);
		i++;
	}
	banquet->genesis = timestamp_ms();
	banquet->live = true;
	return (CLEAR);
}

int	main(int ac, char **av)
{
	t_banquet	bqt;
	t_philos	*phils;
	t_error		error;
	t_fid		fid;

	phils = NULL;
	fid = PARSING;
	error = parse_args(&bqt, ac, av);
	if (!error)
	{
		fid = TABLESETTING;
		error = set_table(&bqt);
	}
	if (!error)
	{
		fid = RECEPTION;
		error = introduce_guests(&phils, &bqt);
	}
	if (!error)
	{
		fid = FEASTING;
		table_check(&bqt, phils);
	}
	return (phi_shutdown(&bqt, phils, error, fid));
}

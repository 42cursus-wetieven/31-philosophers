/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phi_pacing.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/09 11:44:17 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/21 18:06:33 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"

/* int	get_timestamp(const struct timeval origin) */
/* { */
/* 	struct timeval	now; */
/* 	long			origin_ms; */
/* 	long			now_ms; */
/* 	int				result; */

/* 	gettimeofday(&now, NULL); */
/* 	now_ms = now.tv_sec * 1000 + now.tv_usec / 1000; */
/* 	origin_ms = origin.tv_sec * 1000 + origin.tv_usec / 1000; */
/* 	result = now_ms - origin_ms; */
/* 	return (result); */
/* } */

long long	timestamp_ms(void)
{
	struct timeval	time;

	gettimeofday(&time, NULL);
	return (time.tv_sec * 1000 + time.tv_usec / 1000);
}

/* void	punctilious_sleep(int target) */

void	punctilious_sleep(long long target, t_banquet *bqt)
{
	long long	start;

	start = timestamp_ms();
	while (bqt->live && timestamp_ms() - start < target)
		usleep(50);
}

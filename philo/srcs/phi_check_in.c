/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phi_check_in.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/10 16:44:49 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/21 18:09:52 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"
#include "phi_pacing.h"
#include "phi_courses.h"

static bool	party_is_satiated(t_banquet *bqt, t_philos *phils)
{
	int	i;

	i = 0;
	while (bqt->live && i < bqt->phils_count)
	{
		if (phils[i].meal_count < bqt->meal_cap)
			return (false);
		i++;
	}
	bqt->live = false;
	return (true);
}

static bool	someone_starved(t_banquet *bqt, t_philos *phils)
{
	int	i;

	i = 0;
	while (bqt->live && i < bqt->phils_count)
	{
		if (timestamp_ms() - phils[i].previous_meal > bqt->fasting_endurance)
		{
			pthread_mutex_lock(&phils[i].eating);
			statement("died", &phils[i], true);
			pthread_mutex_unlock(&phils[i].eating);
			return (true);
		}
		i++;
	}
	return (false);
}

void	table_check(t_banquet *bqt, t_philos *phils)
{
	while (!bqt->live)
		;
	while (true)
	{
		if (bqt->meal_cap >= 0 && party_is_satiated(bqt, phils))
			break ;
		if (someone_starved(bqt, phils))
			break ;
		usleep(50);
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phi_courses.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/09 11:40:02 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/22 15:12:16 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "philosophers.h"
#include "phi_pacing.h"

void	statement(const char *msg, t_philos *phil, bool closing_statement)
{
	pthread_mutex_lock(&phil->bqt->floor);
	if (phil->bqt->live)
	{
		if (closing_statement)
			phil->bqt->live = false;
		printf("%lli %i %s\n", timestamp_ms() - phil->bqt->genesis,
			phil->key, msg);
	}
	pthread_mutex_unlock(&phil->bqt->floor);
}

static void	digestion(t_philos *phil)
{
	statement("is sleeping", phil, false);
	punctilious_sleep(phil->bqt->nap_time, phil->bqt);
	statement("is thinking", phil, false);
}

static void	nourishment(t_philos *phil)
{
	pthread_mutex_lock(&phil->bqt->forks[phil->key - 1]);
	statement("has taken a fork", phil, false);
	if (phil->bqt->phils_count == 1)
		return ;
	pthread_mutex_lock(&phil->bqt->forks[phil->key % phil->bqt->phils_count]);
	statement("has taken a fork", phil, false);
	pthread_mutex_lock(&phil->eating);
	statement("is eating", phil, false);
	phil->previous_meal = timestamp_ms();
	pthread_mutex_unlock(&phil->eating);
	punctilious_sleep(phil->bqt->meal_time, phil->bqt);
	pthread_mutex_unlock(&phil->bqt->forks[phil->key - 1]);
	pthread_mutex_unlock(&phil->bqt->forks[phil->key % phil->bqt->phils_count]);
	phil->meal_count++;
}

void	*feast(void *philosopher)
{
	t_philos	*phil;

	phil = (t_philos *)philosopher;
	while (!phil->bqt->live)
		;
	if (phil->key % 2 == 0)
		punctilious_sleep(phil->bqt->meal_time, phil->bqt);
	while (phil->bqt->live)
	{
		nourishment(phil);
		if (phil->bqt->phils_count == 1)
			return (NULL);
		digestion(phil);
	}
	return (NULL);
}
